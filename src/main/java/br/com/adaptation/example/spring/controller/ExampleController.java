package br.com.adaptation.example.spring.controller;

import lombok.extern.log4j.Log4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.adaptation.core.spring.annotation.LocaleMessageSource;
import br.com.adaptation.core.spring.helper.MessageLocaleHelper;

@Controller
@Log4j
@LocaleMessageSource(basename = "module_example_example")
public class ExampleController {

	@Autowired
	MessageLocaleHelper message;
	
	@RequestMapping(value = "/example", method = RequestMethod.GET)
	public String index() {
		
		// COM PARAMETRO
		String[] params = {"TESTE"};
		message.success("module.example.example.index.message", params);
		
		// SEM PARAMETRO
		//message.success("module.example.example.index.message");
		
		return "module/example/controller/example/index";
	}

}